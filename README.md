#README / LEEME

*Práctica:* 10

*Unidad:* UD03. Responsive Design

*Módulo:* Diseño de Interfaces WEB

*Ciclo Formativo:* Desarrollo de Aplicaciones WEB

*Objetivo:* Adaptar un layout clásico (cabecera, columnas, pie de página)

##Author / Autor

Alejandro Amat
Adaptada por Alfredo Oltra (mail: [alfredo.ptcf@gmail.com](alfredo.ptcf@gmail.com) / twitter: [@aoltra](https://twitter.com/aoltra))

